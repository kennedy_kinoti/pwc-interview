#Merchant Registration API

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
- GO v1.10.4
- `chi` Routing Package
- `dep` Dependancy Manager
- `MySQL` Database
- `dotenv` dotenv


##Installing
- Copy files to your local machine
- Navigate to the file path in your computer
- You also need to have the following GO packages installed
    
    ```
    go1.10.4
    ```

- Using the existing .env.example template, configure your environment variables and save the file .env

## Launching the program
- Run a go run . command in your terminal and open :8090

## API End Points

#### Create Affiliate
    Path : /affiliate/create
    Method: POST
    Payload: 
    ```{
        "business_name": "iPay Ltd",
        "industry": [
            "Airlines/Aviation"
        ],
        "phone": "724419446",
        "email": "kennedy@ipayafrica.com",
        "password": "a",
        "business_type": "Sole Proprietorship",
        "rc_code": "rc code",
        "terms": true,
        "is_duplicate": false
    }```

    Response: 201

#### View All Affiliates
    Path : /affiliate
    Method: GET
   
    Response: 201

#### View Affiliate by ID
    Path : /affiliate/{ID}
    Method: GET
   
    Response: 201

## Running the tests
Onging development and deployment tests

## Deployment
Not yet deployed to a live system

## Built With
- GoLang - The core backend language

## Authors
Kennedy Kinoti

## Acknowledgments
Hat tip to anyone whose code was used

As Winston Churchill said:
> To improve is to change, to perfect is to change often.

