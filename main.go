package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

var router *chi.Mux
var db *sql.DB

// Company details struct
type Company struct {
	ID             int
	FakeName       string `json:"fake_company_name"`
	Description    string `json:"description"`
	Tagline        string `json:"tagline"`
	CompanyEmail   string `json:"company_email"`
	BusinessNumber string `json:"business_number"`
	Restricted     string `json:"restricted"`
}

type ACompany struct {
	ID           int
	BusinessType string   `json:"business_type"`
	BusinessName string   `json:"business_name"`
	Industry     []string `json:"industry"`
	Phone        string   `json:"phone"`
	Email        string   `json:"email"`
	Password     string   `json:"password"`
	RcCode       string   `json:"rc_code,omitempty"`
	Terms        bool     `json:"terms"`
	IsDuplicate  bool     `json:"is_duplicate"`
}

// IndustryString to array
type IndustryString struct {
	Industry string `json:"industry"`
}

// EmailData struct
type EmailData struct {
	Name    string
	Subject string
	Body    string
	URL     string
}

var testTemplate *template.Template

func init() {
	router = chi.NewRouter()
	router.Use(middleware.Recoverer)

	var err error

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	dbName := os.Getenv("dbName")
	dbPass := os.Getenv("dbPass")
	dbHost := os.Getenv("dbHost")
	dbPort := os.Getenv("dbPort")

	dbSource := fmt.Sprintf("root:%s@tcp(%s:%s)/%s?charset=utf8", dbPass, dbHost, dbPort, dbName)
	db, err = sql.Open("mysql", dbSource)
	catch(err)
}

func routers() *chi.Mux {

	router.Handle("/", http.FileServer(http.Dir("./static")))

	router.Get("/ping", ping)
	router.Get("/company", AllCompanys)
	router.Get("/company/{id}", DetailCompany)
	router.Get("/company/restricted", RestrictedCompanys)
	router.Post("/company/create", CreateCompany)
	// router.Get("/affiliate/validate/", IsDuplicate)

	return router
}

// server starting point
func ping(w http.ResponseWriter, r *http.Request) {
	respondwithJSON(w, http.StatusOK, map[string]string{"message": "Welcome to Company Status API"})
}

//-------------- API ENDPOINT ------------------//

// AllCompanys get all the companys
func AllCompanys(w http.ResponseWriter, r *http.Request) {
	errors := []error{}
	payload := []Company{}

	rows, err := db.Query("Select * From faux_id_fake_companies")
	catch(err)

	defer rows.Close()

	for rows.Next() {
		data := Company{}
		// inda := IndustryString{}

		er := rows.Scan(
			&data.ID,
			&data.FakeName,
			&data.Description,
			&data.Tagline,
			&data.CompanyEmail,
			&data.BusinessNumber,
			&data.Restricted,
		)

		// data.Industry = strings.Split(inda.Industry, ",")

		// fmt.Println(data)

		if er != nil {
			errors = append(errors, er)
		}
		payload = append(payload, data)
	}

	respondwithJSON(w, http.StatusOK, payload)
}

// DetailCompany get specific company details
func DetailCompany(w http.ResponseWriter, r *http.Request) {
	payload := Company{}
	// inda := IndustryString{}

	id := chi.URLParam(r, "id")
	// fmt.Println(id)

	row := db.QueryRow("Select * From faux_id_fake_companies where `business_number`=?", id)

	err := row.Scan(
		&payload.ID,
		&payload.FakeName,
		&payload.Description,
		&payload.Tagline,
		&payload.CompanyEmail,
		&payload.BusinessNumber,
		&payload.Restricted,
	)

	// payload.Industry = strings.Split(inda.Industry, ",")

	if err != nil {
		respondWithError(w, http.StatusNotFound, "no rows in result set")
		return
	}

	respondwithJSON(w, http.StatusOK, payload)
}

// RestrictedCompanys get all the companys
func RestrictedCompanys(w http.ResponseWriter, r *http.Request) {
	errors := []error{}
	payload := []Company{}

	rows, err := db.Query("SELECT * FROM `faux_id_fake_companies` WHERE `restricted` = 'Yes'")
	catch(err)

	defer rows.Close()

	for rows.Next() {
		data := Company{}
		// inda := IndustryString{}

		er := rows.Scan(
			&data.ID,
			&data.FakeName,
			&data.Description,
			&data.Tagline,
			&data.CompanyEmail,
			&data.BusinessNumber,
			&data.Restricted,
		)

		// data.Industry = strings.Split(inda.Industry, ",")

		// fmt.Println(data)

		if er != nil {
			errors = append(errors, er)
		}
		payload = append(payload, data)
	}

	respondwithJSON(w, http.StatusOK, payload)
}

// DateStamp Logs folder
func LogFolder() string {
	// Log file name
	dt := time.Now()

	// log file path
	newpath := filepath.Join(".", "logs")

	os.Mkdir(newpath, os.ModePerm)

	return newpath + "/" + dt.Format("2006-01-02") + ".log"
}

// CreateCompany create a new company
func CreateCompany(w http.ResponseWriter, r *http.Request) {
	company := Company{}
	json.NewDecoder(r.Body).Decode(&company)

	filename := LogFolder()

	f, err := os.OpenFile(filename,
		os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening log file: %v", err)
		log.Println("error opening log file", err)
	}
	defer f.Close()

	log.SetOutput(f)

	// industryString := strings.Join(company.Industry, ", ")

	fmt.Printf("User doesn't exist... Creating ")
	log.Println("Create Company API:")

	go SendEmail(company)

	query, err := db.Prepare("Insert company_registration SET business_type=?, business_name=?, industry=?, phone=?,  email=?, password=?, rc_code=?, terms=?")
	log.Println(map[string]string{"error": "sample message", "status": "false"})
	catch(err)

	_, er := query.Exec(company.ID, company.FakeName, company.Description, company.Tagline, company.CompanyEmail, company.BusinessNumber, company.Restricted)
	catch(er)

	if er != nil {
		log.Fatal("Cannot run insert statement", er)
	}

	defer query.Close()

	respondwithJSON(w, http.StatusCreated, map[string]string{"message": "company successfully created", "status": "true"})

	log.Println(map[string]string{"message": "company successfully created", "status": "true"})

}

// SendEmail HTML to new Company
func SendEmail(m Company) {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	url := os.Getenv("emailURL")
	from := os.Getenv("emailFrom")
	emailName := os.Getenv("emailFromName")
	emailAPI := os.Getenv("emailAPIKey")

	postData := make(map[string]interface{})
	postData["from"] = from
	postData["fromName"] = emailName
	postData["apikey"] = emailAPI
	postData["subject"] = "iPay Company, Karibu"

	messages := make(chan string)

	go postCurl(messages, url, postData)
}

// postCurl
func postCurl(messages chan<- string, url string, postFields map[string]interface{}) {

	postFieldsString := ""

	for k, v := range postFields {
		if postFieldsString == "" {
			postFieldsString = k + "=" + fmt.Sprintf("%v", v)
		} else {
			postFieldsString = postFieldsString + "&" + k + "=" + fmt.Sprintf("%v", v)
		}
	}

	// log.Println(postFieldsString)

	payload := strings.NewReader(postFieldsString)
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("cache-control", "no-cache")
	res, _ := http.DefaultClient.Do(req)

	if res != nil {
		defer res.Body.Close()
	}

	body, _ := ioutil.ReadAll(res.Body)

	response := string(body)
	// fmt.Println(response)
	messages <- response
}

func main() {
	routers()

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading port configuration from ENV")
	}

	port := os.Getenv("port")

	http.ListenAndServe(":"+port, Logger())

}
